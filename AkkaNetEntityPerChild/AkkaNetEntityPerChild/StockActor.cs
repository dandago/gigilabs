﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetEntityPerChild
{
    public class StockActor : ReceiveActor
    {
        private string symbol;

        public StockActor(string symbol)
        {
            this.symbol = symbol;

            this.Receive<decimal>(Handle, null);
        }

        private void Handle(decimal price)
        {
            Console.WriteLine($"{Context.Self.Path} - {this.symbol}: {price} ");
        }
    }
}
