﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubSubExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var publisher = new Publisher();

            var subscriber1 = new Subscriber();
            var subscriber2 = new Subscriber();
            var subscriber3 = new Subscriber();

            publisher.Subscribe(subscriber1);
            publisher.Subscribe(subscriber2);

            publisher.NotifyAll("Hello!");

            publisher.Subscribe(subscriber3);
            publisher.Unsubscribe(subscriber1);

            publisher.NotifyAll("How are you?");

            Console.ReadLine();
        }
    }
}
