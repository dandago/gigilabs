﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeNavigationProperties
{
    public class MultiUserApplication
    {
        private Session session;

        public string SessionId
        {
            get
            {
                return this.session?.Id;
            }
        }

        public MultiUserApplication()
        {

        }
    }
}
