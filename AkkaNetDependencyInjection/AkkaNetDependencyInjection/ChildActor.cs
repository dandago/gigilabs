﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetDependencyInjection
{
    public class ChildActor : ReceiveActor
    {
        public ChildActor(ILazyAss lazyAss)
        {
            Console.WriteLine("Child Actor created!");
        }
    }
}
