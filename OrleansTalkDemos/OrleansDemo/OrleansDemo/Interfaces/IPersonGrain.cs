﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansDemo.Interfaces
{
    public interface IPersonGrain : IGrainWithStringKey
    {
        Task SayHelloAsync();
        Task SleepAsync();
    }
}
