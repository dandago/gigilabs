﻿using Orleans;
using OrleansDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansDemo.Grains
{
    public class PersonGrain : Grain, IPersonGrain
    {
        public Task SayHelloAsync()
        {
            Console.WriteLine($"{this.GetPrimaryKeyString()} says hi!");

            return Task.CompletedTask;
        }

        public Task SleepAsync()
        {
            Console.WriteLine($"{this.GetPrimaryKeyString()} is going to sleep!");

            this.DeactivateOnIdle();

            return Task.CompletedTask;
        }

        #region Life cycle hooks

        public override async Task OnActivateAsync()
        {
            string grainId = this.GetPrimaryKeyString();

            Console.WriteLine($"{grainId} activated!");

            var personRegistryGrain = GrainFactory.GetGrain<IPersonRegistryGrain>("Registry");
            await personRegistryGrain.RegisterGrainAsync(grainId);

            await base.OnActivateAsync();
        }

        public override async Task OnDeactivateAsync()
        {
            string grainId = this.GetPrimaryKeyString();

            Console.WriteLine($"{this.GetPrimaryKeyString()} deactivated!");

            var personRegistryGrain = GrainFactory.GetGrain<IPersonRegistryGrain>("Registry");
            await personRegistryGrain.UnregisterGrainAsync(grainId);

            await base.OnDeactivateAsync();
        }

        #endregion Life cycle hooks
    }
}
