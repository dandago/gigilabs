﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansStatelessWorkerDashboard.Interfaces
{
    public interface IWorkerGrain : IGrainWithStringKey
    {
        Task WorkAsync();
    }
}
