﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansDependencyInjection
{
    public interface IHello : IGrainWithIntegerKey
    {
        Task HelloAsync();
    }
}
