﻿using Akka.Actor;
using Akka.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetCustomLogger.Loggers
{
    public class ConsoleLogger : ReceiveActor
    {
        public ConsoleLogger()
        {
            Receive<Debug>(e => this.Log(LogLevel.DebugLevel, e.ToString()));
            Receive<Info>(e => this.Log(LogLevel.InfoLevel, e.ToString()));
            Receive<Warning>(e => this.Log(LogLevel.WarningLevel, e.ToString()));
            Receive<Error>(e => this.Log(LogLevel.ErrorLevel, e.ToString()));
            Receive<InitializeLogger>(_ => this.Init(Sender));
        }

        protected override void PreStart()
        {
            base.PreStart();

            using (var consoleColour = new ScopedConsoleColour(ConsoleColor.Green))
                Console.WriteLine("PreStart");
        }

        protected override void PostStop()
        {
            using (var consoleColour = new ScopedConsoleColour(ConsoleColor.Green))
                Console.WriteLine("PostStop");

            base.PostStop();
        }

        private void Init(IActorRef sender)
        {
            using (var consoleColour = new ScopedConsoleColour(ConsoleColor.Green))
                Console.WriteLine("Init");

            sender.Tell(new LoggerInitialized());
        }

        private void Log(LogLevel level, string message)
        {
            ConsoleColor colour = ConsoleColor.Gray;

            switch (level)
            {
                case LogLevel.DebugLevel:
                    colour = ConsoleColor.Gray;
                    break;
                case LogLevel.InfoLevel:
                    colour = ConsoleColor.White;
                    break;
                case LogLevel.WarningLevel:
                    colour = ConsoleColor.Yellow;
                    break;
                case LogLevel.ErrorLevel:
                    colour = ConsoleColor.Red;
                    break;
                default: // shouldn't happen
                    goto case LogLevel.InfoLevel;
            }

            using (var consoleColour = new ScopedConsoleColour(colour))
                Console.WriteLine(message);
        }
    }
}
