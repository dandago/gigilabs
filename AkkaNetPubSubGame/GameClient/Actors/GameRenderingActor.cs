﻿using Akka.Actor;
using GameClient.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameClient.Actors
{
    public class GameRenderingActor : TypedActor,
        IHandle<DrawMessage>,
        IHandle<EraseMessage>
    {
        public void Handle(DrawMessage message)
        {
            Console.SetCursorPosition(message.X, message.Y);
            Console.Write(message.Char);
        }

        public void Handle(EraseMessage eraseMessage)
        {
            var x = eraseMessage.X;
            var y = eraseMessage.Y;
            char @char = ' '; // overwrite with space to erase whatever is drawn

            var drawMessage = new DrawMessage(x, y, @char);
            Self.Tell(drawMessage);
        }
    }
}
