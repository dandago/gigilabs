﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameMessages
{
    public class UpdateLocationMessage
    {
        public char Avatar { get; }
        public short OldX { get; }
        public short OldY { get; }
        public short NewX { get; }
        public short NewY { get; }

        public UpdateLocationMessage(Guid subscriberGuid,
            char avatar, short oldX, short oldY, short newX, short newY)
        {
            this.Avatar = avatar;
            this.OldX = oldX;
            this.OldY = oldY;
            this.NewX = newX;
            this.NewY = newY;
        }
    }
}
