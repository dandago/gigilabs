﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameMessages
{
    public class UnsubscribeMessage
    {
        public Guid SubscriberGuid { get; }
        public short LastX { get; }
        public short LastY { get; }

        public UnsubscribeMessage(Guid subscriberGuid,
            short lastX, short lastY)
        {
            this.SubscriberGuid = subscriberGuid;
            this.LastX = lastX;
            this.LastY = lastY;
        }
    }
}
