﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var configManager = ConfigurationManager.AppSettings;
            string actorSystemName = configManager["actorSystemName"];

            Console.Title = $"{actorSystemName} - Server";

            try
            {
                using (var actorSystem = ActorSystem.Create(actorSystemName))
                {
                    var server = actorSystem.ActorOf(
                        Props.Create<ServerActor>(), "ServerActor");

                    string serverActorAddress = configManager["serverActorAddress"];
                    var remoteServerActor = actorSystem.ActorSelection(serverActorAddress);

                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
