﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetReceiveTImeout
{
    public class SomeActor : ReceiveActor
    {
        public SomeActor()
        {
            this.Receive<string>(Handle, null);
            this.Receive<ReceiveTimeout>(Handle, null);

            var timeout = TimeSpan.FromSeconds(3);
            this.SetReceiveTimeout(timeout);
        }

        private void Handle(string msg)
        {
            Console.WriteLine(msg);
        }

        private void Handle(ReceiveTimeout msg)
        {
            Console.WriteLine("Timeout received!");

            Context.Stop(Context.Self); // stop the actor
        }
    }
}
