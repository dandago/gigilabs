﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MultiLevelCachePoc.Utility
{
    public interface IMultiLevelCacheRepository
    {
        Task<List<string>> GetLanguagesAsync();
    }
}